%{
f = @(v) (v(1)^2 - 6*v(1) + 2*v(2)^2 - 2*v(2) + 12*v(3)^2 + v(1)*v(2));
gradf3 = @(v) [(2*v(1) -6 +v(2)), (4*v(2) - 2 + v(1)), 24*v(3)]';
hess3 = @(v) [2,1,0;1,4,0;0,0,24];

x03 = [0,0,0]';
%}

f = @(v) (v(1)^2 - 6*v(1) + 2*v(2)^2 - 2*v(2) + v(1)*v(2));
gradf = @(v) [(2*v(1) -6 +v(2)), (4*v(2) - 2 + v(1))]';
hess = @(v) [2,1;1,4];

x0 = [0,0]';

xnewton2 = metNewton(gradf,hess,x0,5);

%xnewton3 = metNewton(gradf3,hess3,x03,5);

%segun wolfram alpha la solucion es [22/7, -2/7, 0]