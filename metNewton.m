function xmat = metNewton(gradf,hess,x0,N)
%Metodo de newton multivariable. tomamos de argumentos f, grad y hess, x0
%   EL VECTOR x0 ha de ser un vector COLUMNA

dimension = length(x0);
xmat = zeros(dimension,N);
xmat(:,1) = x0;

for i=1:N-1
  xmat(:,i+1) = xmat(:,i) - hess(xmat(:,i))\gradf(xmat(:,i));
end

end

