%Metodo de newton para el problema usado

x0 = [1,1]'; %suelo hacerlo con vectores fila pero aquí voy a usar columnas porque sí
N = 20;
xmat = zeros(2,N);
xmat(:,1) = x0;


%POR ALGUN MOTIVO hay que ponerle parentesis para que te reconozca dos
%componentes de un vector
gradf1 = @(x,y) [(4*x^3 -2*x*y^2) , (-2*y*x^2 + 4*y^3)]';
hess1 = @(x,y) [(12*x^2 -2*y^2) , (-4*x*y) ; (-4*x*y) , (12*x^2 -2*y^2)];
%si este ejercicio sirve para algo que sea para entender estas cosas de
%matlab. una funcion f(x,y) no es lo mismo que f([x,y])

f =@(x,y) (x^4 - (x*y)^2 + y^4);
gradf = @(x) [(4*x(1)^3 -2*x(1)*x(2)^2) , (-2*x(2)*x(1)^2 + 4*x(2)^3)]';
hess = @(x) [(12*x(1)^2 -2*x(2)^2) , (-4*x(1)*x(2)) ; (-4*x(1)*x(2)) , (12*x(1)^2 -2*x(2)^2)];


for i=1:N-1
  xmat(:,i+1) = xmat(:,i) - inv(hess(xmat(:,i)))*gradf(xmat(:,i));
end

x0 = [1,1]';
xmat2 = zeros(2,N);
xmat2(:,1) = x0;

for i=1:N-1
  xmat2(:,i+1) = xmat2(:,i) - hess(xmat2(:,i))\gradf(xmat2(:,i)); %pruebo el método \ para division matricial por recomendacion del interprete
end