%vamos a probar a implementar el metodo del gradiente. WIP!!!
% recordemos que los lambdas son positivos 

f = @(v) (v(1)^2 + 2*v(2)^2 - 2*v(2) - 2*v(1)*v(2));
gradf = @(v) [(2*v(1) - 2*v(2)) , (4*v(1) - 2 - 2*v(1))]';
hess = @(v) [2,-2 ; -2, 4];


N = 10;
lambdas = linspace(0,10,100)';
numeroLambdas = length(lambdas);

x0 = [0,0]';

%{
probamos el metodo de newton
%}
xnewton = metNewton(gradf,hess,x0,N);
%%%%%%%%%%%%%%%%%%%%%%

% pasos. iniciamos con x0 = [0,0]'

lambd_valoresf = zeros(numeroLambdas,2);

x = x0;
d = -gradf(x0); %cuando hacemos el algoritmo a mano solemos cambiar d 
%un poco para facilitar el calculo pero aqui lo dejo tal cual

for j=1:N %numero de iteraciones

for i=1:100
    if f(x + lambdas(i)*d) > 0
        lambd_valoresf(i,:) = [lambdas(i),f(x + lambdas(i)*d)];
    else
        lambd_valoresf(i,:) = [lambdas(i),Inf];
    end
end

minimof = min(lambd_valoresf(:,2));
index = find(lambd_valoresf(:,2) == minimof);
lambda = lambdas(index);

x = x + lambda*d ;

end
